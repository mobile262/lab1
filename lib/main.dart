import 'package:flutter/material.dart';

void main () => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {

  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();

}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String germanGreeting = "Hallo Flutter";
String franceGreeting= "Bonjour Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {

  String displayText = englishGreeting;

    Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.change_circle,
                  color: Colors.black,
                  size: 25.0,
                ),
            ),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    germanGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.change_circle_outlined,
                color: Colors.red,
                size: 25.0,
                ),
            ),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                        franceGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.change_circle_rounded,
                color: Colors.amberAccent,
                size: 25.0,
                ),
            ),
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}






















// class HelloFlutterApp extends StatelessWidget{
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: (){},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello flutter",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }





